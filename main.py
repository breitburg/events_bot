from telebot import *
from random import randint
from json import loads, dumps
token = ''
bot = TeleBot(token)

criterias = ['Мафия', 'Монополия', 'Покер',
			 'Программирование', 'Волейбол', 'Карты']
states = {}

events_creation = {}

events = loads(open('events.json', 'r').read())
save_events = lambda: open('events.json', 'w').write(dumps(events))

users = loads(open('users.json', 'r').read())
save_users = lambda: open('users.json', 'w').write(dumps(users))

@bot.callback_query_handler(func=lambda call: True)
def callback_query(call):
	if int(call.data) not in events:
		bot.answer_callback_query(call.id, 'Мероприятие не доступно.')
	elif call.message.chat.id in events[int(call.data)]['members']:
		bot.answer_callback_query(call.id, 'Вы уже участвуете.')
	else:
		cid = call.message.chat.id
		mid = call.message.message_id
		bot.edit_message_text((call.message.text).replace(str(len(events[int(call.data)]['members'])) + '/' + str(events[int(call.data)]['count']), str(len(events[int(call.data)]['members']) + 1) + '/' + str(events[int(call.data)]['count'])) + '\nВы участвуете.', cid, mid)
		events[int(call.data)]['members'].append(call.message.chat.id)
		bot.answer_callback_query(call.id, 'Теперь вы участвуете в {}'.format(events[int(call.data)]['title']))

@bot.message_handler(commands=['start', 'menu'])
def send_welcome(message):
	if users[message.from_user.username]['criterias'] == []:
		markup = types.ReplyKeyboardMarkup(one_time_keyboard=False)
		for item in criterias:
			markup.add(types.KeyboardButton(item))
		markup.add('✅')
		users[message.from_user.username]['criteria'] = []
		states[message.chat.id] = 'criterias1'
		bot.send_message(message.chat.id, 'Пожалуйста, выберите интересующие критерии. Когда все нужные критерии будут выбраны нажмите на (✅) в низу списка.', reply_markup=markup); return
	if message.from_user.username not in users: bot.send_message(message.chat.id, 'У вас нет доступа к боту!'); return
	bot.send_message(message.chat.id, 'Добро пожаловать ✌️\n\nЭтот бот позволяет создавать и участвовать в мероприятиях\n\nДоступные команды:\n/create – создать мероприятие\n/list – отобразить все актуальные мероприятия\n/cancel – отменить текущее действие') # , reply_markup=markup

@bot.message_handler(commands=['create'])
def create_event(message):
	if message.from_user.username not in users: bot.send_message(message.chat.id, 'У вас нет доступа к боту!'); return
	bot.send_message(message.chat.id, 'Пожалуйста, напишите название мероприятия...')
	states[message.chat.id] = 'event_creation1'

@bot.message_handler(commands=['cancel'])
def clear_state(message):
	if message.from_user.username not in users: bot.send_message(message.chat.id, 'У вас нет доступа к боту!'); return
	if message.chat.id in states:
		del states[message.chat.id]
	bot.send_message(message.chat.id, 'Мы завершили текущую задачу. Пожалуйста, вернитесь в /menu.')

@bot.message_handler(commands=['list'])
def view_list(message):
	if message.from_user.username not in users: bot.send_message(message.chat.id, 'У вас нет доступа к боту!'); return
	bot.send_message(message.chat.id, 'Всего доступно {events_count} событий:'.format(events_count=str(len(events))))
	for item in events:
		markup = types.InlineKeyboardMarkup()
		markup.add(types.InlineKeyboardButton('Участвовать', callback_data=item))
		bot.send_message(message.chat.id, 'Название: {title}\nОписание: {description}\n{members_count}/{members_needed} участников.'.format(title=events[item]['title'], description=events[item]['description'], members_count=len(events[item]['members']), members_needed=str(events[item]['count'])), reply_markup=markup)

@bot.message_handler(func=lambda m: True)
def echo_all(message):
	if message.from_user.username not in users: bot.send_message(message.chat.id, 'У вас нет доступа к боту!'); return
	if states[message.chat.id] == 'criterias1':
		if message.text not in criterias and message.text != '✅':
			bot.send_message(message.chat.id, 'Не известная критерия. Пожалуйста, выберите из списка.')
			return
		if message.text == '✅':
			bot.send_message(message.chat.id, 'Отлично! Критерии были добавлены. Пожалуйста, перейдтие в /menu.')
			save_users()
			del states[message.chat.id]
			return
		users[message.from_user.username]['criterias'].append(message.text)
		bot.send_message(message.chat.id, 'Добавлена критерия – {criteria}'.format(criteria=message.text))
	elif message.chat.id not in states:
		bot.send_message(message.chat.id, 'Неизвестная команда! Пожалуйста, вернитесь в /menu.')
	elif states[message.chat.id] == 'event_creation1':
		events_creation[message.chat.id] = {'title' : message.text}
		bot.send_message(message.chat.id, 'Отлично! Теперь отправьте краткое описание мероприятия {event_name}...'.format(event_name=events_creation[message.chat.id]['title']))
		states[message.chat.id] = 'event_creation2'
	elif states[message.chat.id] == 'event_creation2':
		events_creation[message.chat.id]['description'] = message.text
		bot.send_message(message.chat.id, 'Супер! Напишите максимальное кол-во человек для {event_name}...'.format(event_name=events_creation[message.chat.id]['title']))
		states[message.chat.id] = 'event_creation3'
	elif states[message.chat.id] == 'event_creation3':
		try:
			events_creation[message.chat.id]['count'] = int(message.text)
		except ValueError:
			bot.send_message(message.chat.id, 'Ошибка конвертации в число. Пожалуйста, попробуйте еще раз.')
			return
		if events_creation[message.chat.id]['count'] > len(users):
			bot.send_message(message.chat.id, 'Введенное число превышает кол-во участников. Пожалуйста, попробуйте еще раз.')
			return
		markup = types.ReplyKeyboardMarkup(one_time_keyboard=False)
		for item in criterias:
			markup.add(types.KeyboardButton(item))
		markup.add('✅')
		events_creation[message.chat.id]['criteria'] = []
		bot.send_message(message.chat.id, 'Понятно. Для мероприятия {event_name} требуется {count} человек. Пожалуйста, выберите критерии подбора. Когда все нужные критерии будут выбраны нажмите на (✅) в низу списка.'.format(event_name=events_creation[message.chat.id]['title'], count=str(events_creation[message.chat.id]['count'])), reply_markup=markup)
		states[message.chat.id] = 'event_creation4'
	elif states[message.chat.id] == 'event_creation4':
		if message.text not in criterias and message.text != '✅':
			bot.send_message(message.chat.id, 'Не известная критерия. Пожалуйста, выберите из списка.')
			return
		if message.text == '✅':
			markup = types.ReplyKeyboardMarkup(one_time_keyboard=True)
			markup.add(types.KeyboardButton('Да'), types.KeyboardButton('Нет'))
			bot.send_message(message.chat.id, 'Отлично! Критерии были добавлены. Опубликовать?', reply_markup=markup)
			states[message.chat.id] = 'event_creation5'
			return
		events_creation[message.chat.id]['criteria'].append(message.text)
		bot.send_message(message.chat.id, 'Добавлена критерия – {criteria}'.format(criteria=message.text))
	elif states[message.chat.id] == 'event_creation5':
		if message.text == 'Да':
			bot.send_message(message.chat.id, 'Мероприятие {event_name} опубликовано. Сейчас начнется рассылка оповещений. Вы можете увидеть свое мероприятие если напишите /list'.format(event_name=events_creation[message.chat.id]['title']))

			# TODO: Сделать рассылку

			while True:
				ident = randint(1000, 9999)
				if ident not in events: break

			events[ident] = events_creation[message.chat.id]
			events[ident]['author'], events[ident]['members'] = message.chat.id, [message.chat.id]
			save_events()
			return
		bot.send_message(message.chat.id, 'Отмена мероприятия {event_name}. Пожалуйста, вернитесь в /menu.'.format(event_name=events_creation[message.chat.id]['title']))
		del states[message.chat.id]
		del events_creation[message.chat.id]

bot.polling(none_stop=True)

